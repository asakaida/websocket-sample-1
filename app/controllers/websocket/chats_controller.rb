class Websocket::ChatsController < WebsocketRails::BaseController
  def create
    message = message()
    broadcast_message :create, message, namespace: :chats
    # send_message :create, message, namespace: :chats
  end
end
