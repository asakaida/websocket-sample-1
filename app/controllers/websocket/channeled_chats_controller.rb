class Websocket::ChanneledChatsController < WebsocketRails::BaseController
  def create
    message = message()
    WebSocketRails[:channeled_chats].trigger(:create, message, namespace: :channeled_chats)
  end
end
