Rails.application.routes.draw do
  root to: 'main#index'

  resources :chats
  resources :channeled_chats
end
